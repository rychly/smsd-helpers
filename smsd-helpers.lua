local _M = {}

function _M.parse_msg_file(msg_file)
	local result = {}
	local file, error = io.open(msg_file, "r")
	if (file == nil) then
		return nil, error
	end
	local in_header, in_report = true, false
	while true do
		local line = file:read("*l")
		-- EOF
		if (line == nil) then
			break
		-- headers end with an empty line
		elseif (in_header and (line == "")) then
			in_header = false
		-- report headers start with the title
		elseif (line == "SMS STATUS REPORT") then
			in_report = true
		-- report headers end with another empty line
		elseif (in_report and (line == "")) then
			in_report = false
		-- headers and in the header parts
		elseif (in_header or in_report) then
			for key, value in line:gmatch("([^:]+):%s*(.*)$") do	-- matches to the end of line, so it is just one iteration
				result[in_report and ("REPORT_" .. key) or key] = value
			end
		else
			result["Message"] = ((result["Message"] == nil) and "" or (result["Message"] .. "\n")) .. line
		end
	end
	io.close(file)
	return result
end

function _M.parse_numberlist_file(numberlist_file)
	local result = {}
	local file = io.open(numberlist_file, "r")
	if (file == nil) then
		return nil, error
	end
	while true do
		local line = file:read("*l")
		-- EOF
		if (line == nil) then
			break
		-- uncommented lines
		elseif (line:sub(1,1) ~= "#") then
			for key, value in line:gmatch("([^#\t]+)%s*#?%s*(.-)$") do	-- matches to the end of line, so it is just one iteration
				result[key] = value
			end
		end
	end
	io.close(file)
	return result
end

function _M.send_message(phone, event_msg_id, message, outgoing_dir)
	local filename = "reaction." .. (event_msg_id or math.random(2^16, 2^17)).. "." .. os.date("%Y-%m-%d-%H-%M-%S") .. '.' .. phone
	local file = io.open(outgoing_dir .. "/" .. filename, "w")
	if (file == nil) then
		return nil, error
	end
	io.output(file)
	io.write(
		"To: " .. phone .. "\n" ..
		"Alphabet: UTF-8\n" ..
		"Report: no\n" ..
		"\n" ..
		message
	)
	io.close(file)
end

return _M
